import pytest
from link_saver.db import get_db


def test_index(client, auth):
    response = client.get('/')
    assert b'login' in response.data

    auth.login()
    response = client.get('/')
    assert b'link_adder' in response.data
    assert b'Add link' in response.data
    assert b'Add new category' in response.data
    assert b'table' in response.data


