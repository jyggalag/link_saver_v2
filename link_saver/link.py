import functools

from datetime import date

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.exceptions import abort

from link_saver.auth import login_required
from link_saver.db import get_db

bp = Blueprint('link', __name__)


def get_link_starred_status(id):
    link = get_db().execute(
        'SELECT id, starred'
        ' FROM link'
        ' WHERE id = ?', (id, )
    ).fetchone()

    if link is None:
        abort(404, "Link id {} doesn't exist.".format(id))
    
    return link


def get_link(id):
    link = get_db().execute(
        'SELECT l.id, l.title, l.created, l.category_id, l.starred, l.url, c.name'
        ' FROM link l LEFT JOIN category c ON l.category_id = c.id'
        ' WHERE l.id = ?', (id, )
    ).fetchone()

    if link is None:
        abort(404, "Link id {} doesn't exist.".format(id))

    return link


def count():
    return get_db().execute(
        'SELECT COUNT(*) FROM link'
    ).fetchone()[0]


@bp.route('/')
@login_required
def index():
    db = get_db()
    links = db.execute(
        'SELECT l.id, l.title, l.created, l.category_id, l.starred, l.url, c.name'
        ' FROM link l LEFT JOIN category c ON l.category_id = c.id'
        ' ORDER BY starred DESC, created DESC'
    ).fetchall()

    categories = db.execute(
        'SELECT * FROM category'
    ).fetchall()

    today = date.today()

    return render_template('link/index.html', links=links, categories=categories, count=count(), today=today)


@bp.route('/create/category', methods=['POST'])
def create_category():
    if request.method == 'POST':
        name = request.form['name']
        error = None

        if not name:
            error = 'Name is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO category (name) VALUES (?)', (name, )
            )
            db.commit()
            flash("Link added")
            return redirect(url_for('link.index'))


@bp.route('/create/link', methods=['POST'])
def create_link():
    if request.method == 'POST':
        title = request.form['title']
        category_id = request.form['category_id']
        url = request.form['url']
        starred = 0
        error = None

        if not title:
            error = 'Title is required.'
        elif not url:
            error = 'Url is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO link (title, category_id, url, starred)'
                ' VALUES (?, ?, ?, ?)', (title, category_id, url, starred)
            )
            db.commit()
            return redirect(url_for('link.index'))


@bp.route('/<int:id>/update', methods=['GET'])
@login_required
def update(id):
    link = get_link_starred_status(id)
    starred = link['starred']
    if starred == 0:
        starred = 1
    elif starred == 1:
        starred = 0
    
    if request.method == 'GET':
        db = get_db()
        db.execute(
            'UPDATE link SET starred = ? WHERE id = ?', (starred, id)
        )
        db.commit()
        return redirect(url_for('link.index'))


@bp.route('/<int:id>/delete', methods=['GET'])
@login_required
def delete(id):
    get_link(id)
    db = get_db()
    db.execute('DELETE FROM link WHERE id = ?', (id, ))
    db.commit()
    return redirect(url_for('link.index'))
