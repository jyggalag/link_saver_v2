import pytest
from flask import g, session
from link_saver.db import get_db


def test_register(client, app):
    assert client.get('/auth/register/register_me_pls').status_code == 200
    response = client.post(
        '/auth/register/register_me_pls', data = {'username': 'a', 'password': 'a', 'email': 'aaa@aaa.pl'}
    )
    assert 'http://localhost/auth/login' == response.headers['Location']

    with app.app_context():
        assert get_db().execute(
            "SELECT * FROM user WHERE username = 'a'",
        ).fetchone() is not None


@pytest.mark.parametrize(('username', 'password', 'email', 'message') ,(
    ('','a','asd@asd.pl',b'Username is required.'),
    ('a', '', 'asd@asd.pl', b'Password is required'),
    ('a', 'a', '', b'Email is required'),
    ('test', 'test', 'test@test.pl', b'already registered'),
))
def test_register_validate_input(client, username, password, email, message):
    response = client.post(
        '/auth/register/register_me_pls',
        data={'username': username, 'password': password, 'email': email}
    )
    assert message in response.data


def test_login(client, auth):
    assert client.get('/auth/login').status_code == 200
    response = auth.login()
    assert response.headers['Location'] == 'http://localhost/'

    with client:
        client.get('/')
        assert session['user_id'] == 1
        assert g.user['username'] == 'test'


@pytest.mark.parametrize(('username', 'password', 'message'), (
    ('a', 'test', b'Incorrect username or password.'),
    ('test', 'a', b'Incorrect username or password.'),
))
def test_login_validate_input(auth, username, password, message):
    response = auth.login(username, password)
    assert message in response.data


def test_logout(client, auth):
    auth.login()

    with client:
        auth.logout()
        assert 'user_id' not in session

